-- Link di riferimento
-- https://www.citusdata.com/blog/2017/01/27/getting-started-with-github-events-data/

CREATE TABLE github_events
(
    event_id bigint,
    event_type text,
    event_public boolean,
    repo_id bigint,
    payload jsonb,
    repo jsonb,
    user_id bigint,
    org jsonb,
    created_at timestamp
);

CREATE TABLE github_users
(
    user_id bigint,
    url text,
    login text,
    avatar_url text,
    gravatar_id text,
    display_login text
);


CREATE INDEX event_type_index ON github_events (event_type);
CREATE INDEX payload_index ON github_events USING GIN (payload jsonb_path_ops);


SELECT create_distributed_table('github_events', 'user_id'); -- 16 shard per nodo
SELECT create_distributed_table('github_users', 'user_id'); -- 16 shard per nodo


-- dove i dati risiedono
SELECT nodename, count(*) as n_shard
FROM pg_dist_shard_placement
GROUP BY nodename;


\copy github_users from data_test/users.csv CSV;
\copy github_events from data_test/large_events.csv CSV;


-- check no data sul nodo coordinator
select pg_size_pretty(pg_relation_size('github_users'));
select pg_size_pretty(pg_relation_size('github_events'));

-- riptere lo stesso check su un nodo worker1


-- esempio di query semplice

SELECT date_trunc('hour', created_at) AS hour,
       sum((payload->>'distinct_size')::int) AS num_commits
    FROM   github_events
    WHERE  event_type = 'PushEvent'
    GROUP BY hour
    ORDER BY hour;

-- esempio di query con join

SELECT login, count(*)
FROM github_events ge
JOIN github_users gu ON ge.user_id = gu.user_id
WHERE event_type = 'CreateEvent' AND payload @> '{"ref_type": "repository"}'
GROUP BY login
ORDER BY count(*) DESC;


--- Esempio di query mixed

-- creazione di tabella non shardata
create table my_users as select * from github_users ;

-- Esecuzione della stessa query con tabella non shardata
SELECT login, count(*)
FROM github_events ge
JOIN my_users mu ON ge.user_id = mu.user_id
WHERE event_type = 'CreateEvent' AND  payload @> '{"ref_type": "repository"}'
GROUP BY login
ORDER BY count(*) DESC;


-- Possibile Workaourd
with ge as (
  select * from github_events
    WHERE event_type = 'CreateEvent' AND  payload @> '{"ref_type": "repository"}'
)
SELECT login, count(*)
FROM ge
JOIN my_users mu ON ge.user_id = mu.user_id
GROUP BY login
ORDER BY count(*) DESC;
