Repo Test pgtraining 12 Marzo 2021

Esempio di installazione e configurazione ambiente shardato 
PostgreSQL/Citus 

# passi da eseguire durante la demo
# Link di riferimento : http://docs.citusdata.com/en/v9.5/installation/multi_machine_debian.html
#                       https://github.com/citusdata

# creazione dell'immagine
sudo docker-compose build --force-rm --no-cache

#crezione dei volumi
./create_external_volumes.sh

#up dei containers
sudo docker-compose up -d

#check dei logs
sudo docker-compose logs -f

#Checke delle macchine e verifica del NAT
sudo docker ps

#test di psql

# macchina master
psql -U enrico.pirozzi -p6432 -h127.0.0.1 testdb


#worker1
psql -U enrico.pirozzi -p6440 -h127.0.0.1 testdb
#worker2
psql -U enrico.pirozzi -p6441 -h127.0.0.1 testdb


#Dalla macchina master eseguira da ambiente psql

\c testdb

SELECT * FROM master_get_active_worker_nodes();

#Pronti per usare citus !!!!

