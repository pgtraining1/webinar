set -e
{ echo "max_locks_per_transaction = 16384"; } |  tee -a "$PGDATA/postgresql.conf" > /dev/null
createuser -s postgres -U user
psql -U postgres -c "alter role postgres password 'SuperSecret';" postgres
pg_ctl -U postgres -D "$PGDATA" -m fast -w stop
pg_ctl -U postgres -D "$PGDATA" start
